from pydantic import BaseModel
from typing import Literal

StatusStr = Literal['COMPLETED', 'IN_QUEUE', 'IN_PROGRESS', 'FAILED']


class Input(BaseModel):
    prompt: str


class RunRequest(BaseModel):
    input: Input
    webhook: str | None = None


class RunResponse(BaseModel):
    id: str
    status: StatusStr


class Output(BaseModel):
    image: str
    seed: float | None = None


class Status(BaseModel):
    delayTime: float
    executionTime: float
    gpu: str
    id: str
    input: Input
    output: list[Output]
    status: StatusStr
