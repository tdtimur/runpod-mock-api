import asyncio
import httpx
import random
import uvicorn

from fastapi import APIRouter, BackgroundTasks, FastAPI
from uuid import UUID
from models import (
    Input,
    Output,
    RunRequest,
    RunResponse,
    Status,
)


app = FastAPI()
router = APIRouter(prefix='/stable-diffusion-v2')

job_id = UUID('d8ddc068-f4a4-48f0-8571-55339a872ebd').__str__()
status = Status(
    delayTime=100,
    executionTime=5000,
    gpu="30",
    id=job_id,
    input=Input(prompt="a photograph of an astronaut riding a horse"),
    output=[
        Output(image="https://rhino-imagecdn-dev-cdn.youmeme.net/ce609133-99f1-412f-9ce6-024bf8049cde.png.transform"
                     ".1000.1000.jpeg", seed=0.5),
        Output(image="https://rhino-imagecdn-dev-cdn.youmeme.net/82ac4a1e-3707-489a-b6fa-adc4562cc029.png.transform"
                     ".1000.1000.jpeg", seed=0.5),
    ],
    status="COMPLETED",
)
run_response = RunResponse(
    id=job_id,
    status="IN_QUEUE",
)


async def process(data: RunRequest) -> None:
    await asyncio.sleep(5 + random.random())
    if data.webhook is not None:
        async with httpx.AsyncClient() as client:
            await client.post(data.webhook, json=status.dict(), timeout=3600)


@router.post("/run")
async def run(data: RunRequest, bg: BackgroundTasks) -> RunResponse:
    # print(data)
    bg.add_task(process, data)
    return run_response


@router.get("/status/{_id}")
async def get_status(_id: str) -> Status:
    return status


@router.post("/webhook")
async def webhook(data: Status) -> Status:
    # print(data)
    return data


if __name__ == "__main__":
    app.include_router(router)
    uvicorn.run(app, loop="uvloop")
